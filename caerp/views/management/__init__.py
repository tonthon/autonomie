def includeme(config):
    config.include(".companies")
    config.include(".kms")
    config.include(".payments")
    config.include(".treasuries")
    config.include(".contributions")
