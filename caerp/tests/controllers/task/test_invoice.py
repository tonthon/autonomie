from caerp.controllers.task.invoice import (
    attach_invoice_to_estimation,
    attach_invoices_to_estimation,
)
from caerp.models.project import Business


def test_attach_invoice_to_estimation(
    get_csrf_request_with_db, mk_estimation, mk_invoice, business
):
    estimation = mk_estimation()
    invoice = mk_invoice()
    invoice_business_id = invoice.business.id

    attach_invoice_to_estimation(get_csrf_request_with_db(), invoice, estimation)
    assert invoice.estimation_id == estimation.id
    assert estimation.business_id == invoice.business_id
    assert estimation.geninv
    attach_invoice_to_estimation(get_csrf_request_with_db(), invoice, estimation)
    assert Business.get(invoice_business_id) is None


def test_attach_invoices_to_estimation(
    get_csrf_request_with_db, mk_estimation, mk_invoice, business
):
    estimation = mk_estimation()
    invoices = [mk_invoice(), mk_invoice()]

    attach_invoices_to_estimation(get_csrf_request_with_db(), estimation, invoices)
    for i in invoices[0], invoices[1]:
        assert i.estimation_id == estimation.id
        assert i.business_id == estimation.business_id
        assert estimation.geninv
