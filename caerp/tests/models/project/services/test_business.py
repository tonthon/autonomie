import pytest

from caerp.models.project.services.business import BusinessService


def test_to_invoice(business, full_estimation):
    full_estimation.status = "valid"
    assert BusinessService.to_invoice(business) == full_estimation.ht


def test_populate_deadlines(business, full_estimation):
    BusinessService.populate_deadlines(business)
    assert len(business.payment_deadlines) == 3
    for deadline in business.payment_deadlines:
        assert deadline.invoiced is False
    assert business.status == "danger"


def test_populate_find_deadline(business):
    BusinessService.populate_deadlines(business)
    id_ = business.payment_deadlines[1].id
    assert BusinessService.find_deadline(business, id_) == business.payment_deadlines[1]


def test_find_deadline_from_invoice(dbsession, business, invoice):
    BusinessService.populate_deadlines(business)
    business.payment_deadlines[1].invoice = invoice
    dbsession.merge(business.payment_deadlines[1])
    dbsession.flush()

    assert (
        BusinessService.find_deadline_from_invoice(business, invoice)
        == business.payment_deadlines[1]
    )


def test_get_customer(business, full_estimation):
    result = BusinessService.get_customer(business)
    assert result == full_estimation.customer


def test_add_invoice_bug1077(
    get_csrf_request_with_db, business, user, customer, full_estimation
):
    result = business.add_invoice(get_csrf_request_with_db(), user)
    assert result.address == customer.full_address
    assert result.business_id == business.id
    assert result.business_type_id == business.business_type.id
    assert result.company == business.project.company
    assert result.project == business.project


def test_add_estimation_bug1077(get_csrf_request_with_db, business, user, customer):
    result = business.add_estimation(get_csrf_request_with_db(), user)
    assert result.address == customer.full_address
    assert result.business_id == business.id
    assert result.business_type_id == business.business_type.id
    assert result.company == business.project.company
    assert result.project == business.project


def test_is_void(business, mk_business, user):
    assert not business.is_void()

    void_business = mk_business(name="test")
    assert void_business.is_void()


def test__get_estimations_to_invoice(business, full_estimation):
    assert BusinessService._get_estimations_to_invoice(business) == []
    business.estimations[0].status = "valid"
    assert BusinessService._get_estimations_to_invoice(business) == [full_estimation]
    business.estimations[0].signed_status = "aborted"
    assert BusinessService._get_estimations_to_invoice(business) == []


# PROGRESS INVOICING
class TestBusinessProgressInvoicingService:
    @pytest.fixture
    def my_business(
        self,
        dbsession,
        mk_business,
        estimation,
        tva,
        mk_task_line_group,
        mk_task_line,
        default_business_type,
    ):
        estimation.deposit = 10
        estimation.line_groups = []
        gr = mk_task_line_group(task=estimation)
        mk_task_line(group=gr, cost=100000, tva=tva.value)
        mk_task_line(group=gr, cost=100000, tva=tva.value)
        gr = mk_task_line_group(task=estimation)
        mk_task_line(group=gr, cost=100000, tva=tva.value)
        mk_task_line(group=gr, cost=100000, tva=tva.value)
        business = mk_business()
        business.estimations = [estimation]
        dbsession.merge(business)
        estimation.business_type_id = default_business_type.id
        estimation.businesses = [business]
        dbsession.merge(estimation)
        dbsession.flush()
        return business

    @pytest.fixture
    def my_business_with_ps(
        self,
        dbsession,
        get_csrf_request_with_db,
        my_business,
        price_study,
        mk_price_study_product,
        mk_price_study_work,
        mk_price_study_work_item,
        tva,
    ):
        estimation = my_business.estimations[0]
        estimation.line_groups = []
        price_study.task = estimation
        dbsession.merge(price_study)
        dbsession.flush()
        request = get_csrf_request_with_db()
        mk_price_study_product(ht=100000, tva=tva, mode="ht")
        mk_price_study_work_item(ht=100000)
        mk_price_study_work_item(ht=100000)
        work = mk_price_study_work(display_details=False, tva=tva)
        mk_price_study_work_item(price_study_work=work)
        price_study.sync_amounts(sync_down=True)
        price_study.sync_with_task(request)

        return my_business

    def test_populate(self, my_business):
        from caerp.models.progress_invoicing import (
            ProgressInvoicingChapterStatus,
            ProgressInvoicingProductStatus,
        )

        # Invoicing Mode is not correct
        with pytest.raises(Exception):
            BusinessService.populate_progress_invoicing_status(my_business)

        my_business.invoicing_mode = my_business.PROGRESS_MODE

        # No valid estimation
        BusinessService.populate_progress_invoicing_status(my_business)
        assert (
            ProgressInvoicingChapterStatus.query()
            .filter_by(business_id=my_business.id)
            .count()
            == 0
        )

        # Correct use case
        my_business.estimations[0].status = "valid"
        BusinessService.populate_progress_invoicing_status(my_business)

        chapter_statuses = (
            ProgressInvoicingChapterStatus.query()
            .filter_by(business_id=my_business.id)
            .all()
        )
        assert len(chapter_statuses) == 2
        for chapter_status in chapter_statuses:
            assert len(chapter_status.product_statuses) == 2

            for st in chapter_status.product_statuses:
                assert st.percent_to_invoice == 90
                assert st.percent_left == 100

    def test_populate_with_price_study(self, my_business_with_ps):
        business = my_business_with_ps

        from caerp.models.progress_invoicing import (
            ProgressInvoicingChapterStatus,
            ProgressInvoicingProductStatus,
            ProgressInvoicingWorkStatus,
            ProgressInvoicingWorkItemStatus,
        )

        # Invoicing Mode is not correct
        with pytest.raises(Exception):
            BusinessService.populate_progress_invoicing_status(business)

        business.invoicing_mode = business.PROGRESS_MODE

        # No valid estimation
        BusinessService.populate_progress_invoicing_status(business)
        assert (
            ProgressInvoicingChapterStatus.query()
            .filter_by(business_id=business.id)
            .count()
            == 0
        )

        # Correct use case
        business.estimations[0].status = "valid"
        BusinessService.populate_progress_invoicing_status(business)

        chapter_statuses = (
            ProgressInvoicingChapterStatus.query()
            .filter_by(business_id=business.id)
            .all()
        )
        assert len(chapter_statuses) == 1
        assert len(chapter_statuses[0].product_statuses) == 3

        product_statuses = (
            ProgressInvoicingProductStatus.query()
            .filter_by(chapter_status_id=chapter_statuses[0].id)
            .all()
        )
        # Un produit + un work avec display_details à False
        assert len(product_statuses) == 2
        for st in product_statuses:
            assert st.percent_to_invoice == 90
            assert st.percent_left == 100

        work_status = (
            ProgressInvoicingWorkStatus.query()
            .filter_by(chapter_status_id=chapter_statuses[0].id)
            .first()
        )
        assert work_status.percent_to_invoice == 90
        assert work_status.percent_left == 100
        assert work_status.locked == True

        work_item_statuses = (
            ProgressInvoicingWorkItemStatus.query()
            .filter_by(work_status_id=work_status.id)
            .all()
        )
        assert len(work_item_statuses) == 2
        for st in work_item_statuses:
            assert st.percent_to_invoice == 90
            assert st.percent_left == 100

        # Test la non duplication
        BusinessService.populate_progress_invoicing_status(business)
        assert (
            ProgressInvoicingChapterStatus.query()
            .filter_by(business_id=business.id)
            .count()
            == 1
        )


def test_add_progress_invoicing_invoice(
    get_csrf_request_with_db,
    dbsession,
    business_with_progress_invoicing,
    user,
    full_estimation,
    mk_estimation,
):
    invoice = BusinessService.add_progress_invoicing_invoice(
        get_csrf_request_with_db(), business_with_progress_invoicing, user
    )
    assert invoice.display_units == 1
    assert invoice.notes == full_estimation.notes
    assert invoice.workplace == full_estimation.workplace
    assert invoice.description == full_estimation.description
    assert invoice.address == full_estimation.address
    assert invoice.payment_conditions == full_estimation.payment_conditions
    assert invoice.start_date == full_estimation.start_date
    assert invoice.estimation == full_estimation

    est = mk_estimation(business_id=business_with_progress_invoicing.id)
    business_with_progress_invoicing.estimations.append(est)
    dbsession.merge(business_with_progress_invoicing)
    invoice = BusinessService.add_progress_invoicing_invoice(
        get_csrf_request_with_db(), business_with_progress_invoicing, user
    )
    assert invoice.estimation_id is None


def test_on_estimation_status_changed(business_with_progress_invoicing):
    business = business_with_progress_invoicing
    # Set it to aborted, no invoice should be expected after that
    business.estimations[0].signed_status = "aborted"
    BusinessService.populate_progress_invoicing_status(business)
    from caerp.models.progress_invoicing import (
        ProgressInvoicingChapterStatus,
    )

    assert (
        ProgressInvoicingChapterStatus.query()
        .filter_by(business_id=business.id)
        .count()
        == 0
    )


def test_on_invoice_postdelete_delete_business(
    get_csrf_request_with_db, dbsession, mk_business, invoice, default_business_type
):
    from caerp.models.project.business import Business

    business = mk_business()
    bid = business.id
    business.invoices = [invoice]
    dbsession.merge(business)
    invoice.business_type_id = default_business_type.id
    invoice.businesses = [business]
    dbsession.delete(invoice)
    BusinessService.on_task_delete(get_csrf_request_with_db(), business, invoice)
    dbsession.flush()
    assert Business.get(bid) is None


# def test_populate_progress_invoicing_lines(
#     business_with_progress_invoicing, progress_invoicing_invoice
# ):
#     invoice = progress_invoicing_invoice
#     # On a déjà eu une facture d'accompte 10000000 * 90/100 * 10/100 * 2
#     assert invoice.total_ht() == 1800000
#     tva_parts = invoice.tva_ht_parts()
#     assert not set(tva_parts.keys()).difference(set([700, 2000]))
#     assert tva_parts[700] == 900000
#     assert tva_parts[2000] == 900000

#     all_status = business_with_progress_invoicing.progress_invoicing_statuses
#     for status in all_status:
#         assert status.percent_left == 90
#         assert len(status.invoiced_elements) == 1
#         assert status.invoiced_elements[0].percentage == 10


# def test_populate_progress_invoicing_with_no_datas(
#     get_csrf_request_with_db, business_with_progress_invoicing, user, full_estimation
# ):
#     invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
#         get_csrf_request_with_db(), user
#     )
#     BusinessService.populate_progress_invoicing_lines(
#         business_with_progress_invoicing, invoice, {}
#     )
#     assert invoice.estimation == full_estimation
#     assert len(invoice.all_lines) == 2


# def test_populate_progress_invoicing_lines_several_times(
#     get_csrf_request_with_db,
#     business_with_progress_invoicing,
#     progress_invoicing_invoice,
#     user,
# ):
#     invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
#         get_csrf_request_with_db(), user
#     )

#     # On construit la structure de données attendues pour la génération des
#     # lignes de prestation
#     # Ici on a un pourcentage différent par produit
#     appstruct = {}
#     for status in business_with_progress_invoicing.progress_invoicing_group_statuses:
#         appstruct[status.id] = {}
#         for index, line_status in enumerate(status.line_statuses):
#             # une ligne à 10%, une ligne à 20%
#             appstruct[status.id][line_status.id] = (index + 1) * 10

#     # On populate notre facture
#     BusinessService.populate_progress_invoicing_lines(
#         business_with_progress_invoicing,
#         invoice,
#         appstruct,
#     )

#     # Check les TaskLine et TaskLineGroup
#     assert len(invoice.line_groups) == 1
#     assert len(invoice.all_lines) == 2
#     assert invoice.total_ht() == 2700000
#     tva_parts = invoice.tva_ht_parts()
#     assert tva_parts[2000] == 900000
#     assert tva_parts[700] == 1800000

#     group_status = business_with_progress_invoicing.progress_invoicing_group_statuses[0]

#     # Check des status de facturation
#     assert group_status.percent_left is None
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[1].percent_left
#         == 80
#     )
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[2].percent_left
#         == 70
#     )

#     # Check du suivi des éléments facturés
#     invoiced_elements = group_status.invoiced_elements

#     assert len(invoiced_elements) == 2
#     assert invoiced_elements[1].percentage is None

#     # ETAPE 2 :
#     # On repasse sur un pourcentage global au groupe
#     appstruct = {}
#     for status in business_with_progress_invoicing.progress_invoicing_group_statuses:
#         appstruct[status.id] = {}
#         for line_status in status.line_statuses:
#             # une ligne à 10%, une ligne à 20%
#             appstruct[status.id][line_status.id] = 10

#     # On populate notre facture
#     BusinessService.populate_progress_invoicing_lines(
#         business_with_progress_invoicing,
#         invoice,
#         appstruct,
#     )

#     # Check les TaskLine et TaskLineGroup
#     assert len(invoice.line_groups) == 1
#     assert len(invoice.all_lines) == 2
#     assert invoice.total_ht() == 1800000
#     tva_parts = invoice.tva_ht_parts()
#     assert tva_parts[2000] == 900000
#     assert tva_parts[700] == 900000

#     group_status = business_with_progress_invoicing.progress_invoicing_group_statuses[0]

#     # Check des status de facturation
#     assert group_status.percent_left == 80
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[1].percent_left
#         == 80
#     )
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[2].percent_left
#         == 80
#     )

#     # Check du suivi des éléments facturés
#     invoiced_elements = group_status.invoiced_elements

#     assert len(invoiced_elements) == 2
#     assert invoiced_elements[1].percentage == 10

#     # ETAPE 3 : on revient en arrière, on vérifie qu'on a pas des résidus de
#     # l'étape 2
#     appstruct = {}
#     for status in business_with_progress_invoicing.progress_invoicing_group_statuses:
#         appstruct[status.id] = {}
#         for index, line_status in enumerate(status.line_statuses):
#             # une ligne à 10%, une ligne à 20%
#             appstruct[status.id][line_status.id] = (index + 1) * 10

#     # On populate notre facture
#     BusinessService.populate_progress_invoicing_lines(
#         business_with_progress_invoicing,
#         invoice,
#         appstruct,
#     )

#     # Check les TaskLine et TaskLineGroup
#     assert len(invoice.line_groups) == 1
#     assert len(invoice.all_lines) == 2
#     assert invoice.total_ht() == 2700000
#     tva_parts = invoice.tva_ht_parts()
#     assert tva_parts[2000] == 900000
#     assert tva_parts[700] == 1800000

#     group_status = business_with_progress_invoicing.progress_invoicing_group_statuses[0]

#     # Check des status de facturation
#     assert group_status.percent_left is None
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[1].percent_left
#         == 80
#     )
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[2].percent_left
#         == 70
#     )

#     # Check du suivi des éléments facturés
#     invoiced_elements = group_status.invoiced_elements

#     assert len(invoiced_elements) == 2
#     assert invoiced_elements[1].percentage is None
#     assert invoiced_elements[0].percentage == 10

#     # ETAPE 4 : on crée une nouvelle facture et on vérifie que le percent_left
#     # à None est bien pris en compte
#     invoice = business_with_progress_invoicing.add_progress_invoicing_invoice(
#         get_csrf_request_with_db(), user
#     )

#     # On construit la structure de données attendues pour la génération des
#     # lignes de prestation
#     # Ici on a un pourcentage configuré niveau produit composé (semble-t-il)
#     appstruct = {}
#     for status in business_with_progress_invoicing.progress_invoicing_group_statuses:
#         appstruct[status.id] = {}
#         for line_status in status.line_statuses:
#             # une ligne à 10%, une ligne à 20%
#             appstruct[status.id][line_status.id] = 10

#     # On populate notre facture
#     BusinessService.populate_progress_invoicing_lines(
#         business_with_progress_invoicing,
#         invoice,
#         appstruct,
#     )
#     group_status = business_with_progress_invoicing.progress_invoicing_group_statuses[0]

#     # Check des status de facturation
#     assert group_status.percent_left is None
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[1].percent_left
#         == 70
#     )
#     assert (
#         business_with_progress_invoicing.progress_invoicing_statuses[2].percent_left
#         == 60
#    )


def test_get_current_invoice(dbsession, business, mk_invoice):
    inv = mk_invoice(business_id=business.id)
    assert BusinessService.get_current_invoice(business) == inv
    inv.status = "valid"
    dbsession.merge(inv)
    assert BusinessService.get_current_invoice(business) is None


def test_get_total_income_ht(dbsession, business, full_invoice):
    business.invoices = [full_invoice]

    assert BusinessService.get_total_income(business) == 0

    full_invoice.status = "valid"
    # project mode is ht
    assert BusinessService.get_total_income(business) == 10000000

    assert BusinessService.get_total_income(business, column_name="ttc") == 12000000


def test_get_total_income_ttc(dbsession, business_with_project_mode_ttc, full_invoice):
    business_with_project_mode_ttc.invoices = [full_invoice]
    full_invoice.status = "valid"
    # project mode is ttc but we display total income in HT anyway

    assert BusinessService.get_total_income(business_with_project_mode_ttc) == 9167000

    assert (
        BusinessService.get_total_income(
            business_with_project_mode_ttc, column_name="ttc"
        )
        == 11000000
    )


def test_get_total_expenses(
    business,
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
):
    assert business.get_total_expenses() == 56
    assert business.get_total_expenses(tva_on_margin=True) == 48
    assert business.get_total_expenses(tva_on_margin=False) == 14


def test_has_nonvalid_expenses(
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
    expense_sheet,
    business,
):
    assert business.has_nonvalid_expenses() == True  # noqa: E712
    expense_sheet.status = "valid"
    assert business.has_nonvalid_expenses() == False  # noqa: E712


def test_get_total_estimated(business, full_estimation):
    full_estimation.business_id = business.id
    assert business.get_total_estimated() == 0

    full_estimation.status = "valid"
    assert business.get_total_estimated() == 20000000

    full_estimation.signed_status = "aborted"
    assert business.get_total_estimated() == 0

    full_estimation.signed_status = "signed"
    assert business.get_total_estimated() == 20000000


def test_get_total_margin(
    business,
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
    full_invoice,
):
    assert business.get_total_margin() == -56 * 1000

    full_invoice.status = "valid"
    business.invoices = [full_invoice]
    assert business.get_total_margin() == 10000000 - 56 * 1000
    assert business.get_total_margin(tva_on_margin=True) == 12000000 - 48 * 1000
