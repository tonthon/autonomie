from datetime import datetime

import pytest
from freezegun import freeze_time

from caerp.models.activity import (
    Event,
    Activity,
    Attendance,
    get_activity_years,
)


@pytest.fixture
def mk_activity(fixture_factory):
    return fixture_factory(Activity)


@pytest.fixture
def activity(mk_activity):
    return mk_activity()


def test_user_status(activity):
    attendance = Attendance(account_id=1)
    attendance.status = "registered"
    activity.attendances = [attendance]
    assert activity.user_status(1) == "Attendu"
    assert activity.user_status(2) == "Statut inconnu"

    assert activity.is_participant(1)
    assert not activity.is_participant(2)


@freeze_time("2023")
def test_activity_years(date_20190101, date_20200101, mk_activity):
    min_time = datetime.min.time()
    mk_activity(datetime=datetime.combine(date_20200101, min_time))
    mk_activity(datetime=datetime.combine(date_20190101, min_time))
    mk_activity(datetime=datetime.combine(date_20190101, min_time))

    years = get_activity_years()
    assert years == [2019, 2020, 2023]
