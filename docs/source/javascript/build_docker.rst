Compilation javascript avec docker-compose
============================================

Des fichiers de configuration sont fournis pour permettre la compilation
des ressources Javascript à l'aide d'image docker.


Pré-requis
-----------

- Disposer de docker et docker-compose, (et avoir ajouté son utilisateur au groupe docker)


Compiler les fichiers une seule fois (dev et prod)
--------------------------------------------------

.. code-block::

     docker-compose -f js-docker-compose.yaml run --rm webpack-vue npm run dev
     docker-compose -f js-docker-compose.yaml run --rm webpack-marionette npm run dev
     docker-compose -f js-docker-compose.yaml run --rm webpack-vue npm run prod
     docker-compose -f js-docker-compose.yaml run --rm webpack-marionette npm run prod


Compilation avec rechargement dynamique (pour le développement Js)
-------------------------------------------------------------------

.. code-block::

    docker-compose -f js-docker-compose.yaml up


Ajouter / mettre à jour des dépendances (Partie VueJS)
------------------------------------------------------

Les dépendances npm sont installées / mises à jour au moment du lancement du
docker (cf fichier jsDockerFile). **La commande npm install** n'a aucun effet
sur l'intérieur du docker

Pour ajouter/modifier/retirer des dépendances npm à la partie VueJS :

- modifier vue_sources/package.json pour modifier les dépendances
- couper le docker de build vuejs puis le relancer
