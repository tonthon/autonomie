Notifications
==============

.. image:: ./_static/process_notification.png
   :width: 100%
   :alt: Process d'envoi des notifications


CAERP envoie des notifications via différents canaux (NotificationChannel) :

- e-mail
- message (cloche en haut à droite)
- header_message (bandeau en haut de page)
- alert (modale)


Les notifications peuvent être envoyées immédiatement ou programmées plus tard.

Envoi de notification
----------------------

L'envoi de notification se fait en construisant un objet `caerp.utils.notification.abstract::AbstractNotification` et en appelant l'utilitaire `caerp.utils.notification.notification::notify`

.. code-block:: python

    from caerp.utils.nofitication import AbstractNotification, notify

    abstract = AbstractNotification(body="Corps de message", title="Titre", key="message:internal")
    notify(request, abstract, group_names=['manager', 'admin'])


Destinataires
.............

Les notifications peuvent être envoyées à :

- Des groupes
- Des user_ids
- Une enseigne
- Le référent d'un compte de gestion sociale

Programmation d'une notification
.................................

Une notification peut être programmée en spécifiant un **due_datetime**

.. code-block:: python

    from caerp.utils.nofitication import AbstractNotification, notify
    import datetime

    abstract = AbstractNotification(body="Corps de message", title="Titre", key="message:internal", due_datetime=datetime.datetime.now() + datetime.timedelta(days=180))
    notify(request, abstract, group_names=['manager', 'admin'])

Un `caerp.models.notification.notification::NotificationEvent` est alors créé.

Type de notification et Channel d'envoi
.........................................

Une notification est typée, les types sont créés dans le populate (d'autres types peuvent être créés par des plugins extérieurs).
Lors de l'envoi d'une notification on précise le type de notification via une clé.

Le système de notification choisit alors le canal d'envoi à utiliser.

Pour l'instant le Channel d'envoi par défaut du type de la notification est utilisé.

Il est possible de forcer le canal d'envoi

.. code-block:: python

    notify(request, abstract, group_names=['manager', 'admin'], force_channel='email')

Les Channels existant

- email
- message
- header_message
- alert

Ligne de commande
------------------

.. code-block:: command

    caerp-admin config.ini notify --help
    usage: caerp-admin config_uri notify [-h] [-g GROUPS [GROUPS ...]] [--uids [UIDS ...]] [-f FILENAME] [--title TITLE] [--body BODY] [-c CHANNEL] [--versions VERSIONS [VERSIONS ...]] [-k EVENT_KEY] [--from FROM_TIME] [--until UNTIL_TIME]

    optional arguments:
      -h, --help            show this help message and exit
      -g GROUPS [GROUPS ...], --groups GROUPS [GROUPS ...]
                            Nom des groupes utilisateur
      --uids [UIDS ...]     Identifiants du destinataire
      -f FILENAME, --filename FILENAME
                            Chemin vers un fichier json de version (caerp:release_notes.json)
      --title TITLE         titre de la notification
      --body BODY           Message
      -c CHANNEL, --channel CHANNEL
                            Force le canal de communication (email, header_message, alert, message), Défaut: alert
      --versions VERSIONS [VERSIONS ...]
                            Versions à inclure dans la notification (compatible uniquement si on utilise les release notes). Défaut: la dernière
      -k EVENT_KEY, --event-key EVENT_KEY
                            Type d'évènement, doit correspondre à un des types prédéfinis par caerp ou un de ses plugins (voir models/populate). Défaut: 'message:system'
      --from FROM_TIME      Date d'apparition de la notification (format 2023-12-05 12:00:00)
      --until UNTIL_TIME    Date de fin d'apparition de la notification (format 2023-12-05 12:00:00)


Utilisation d'une notification via un fichier html aux groupes admin et manager

.. code-block:: command

    caerp-admin config.ini notify --groups admin manager -f message.html

Publication des notes de version pour 2024.3.0 et 2024.2.0

.. code-block:: command

    caerp-admin config.ini notify --groups admin manager contractor -f caerp:static/release_notes.json --version 2024.3.0 2024.2.0

Publication d'un message en haut de page jusqu'à une certaine date

.. code-block:: command

    caerp-admin config.ini notify --groups admin manager contractor --title "Maintenance ce jeudi 12 septembre" --body "Le service sera interrompu pendant environs une heure." --until "2023-09-13 01:00:00"
