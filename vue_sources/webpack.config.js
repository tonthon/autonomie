const webpack = require('webpack')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')
const path = require('path')

const APP_DIR = path.resolve(__dirname, 'src')
const NODE_MODULES_DIR = path.resolve(__dirname, 'node_modules')
let BUILD_DIR
let SOFT_NAME

if (process.env.CAERP_JS_BUILD_PATH) {
  BUILD_DIR = path.resolve(process.env.CAERP_JS_BUILD_PATH)
} else {
  BUILD_DIR = path.resolve(__dirname, '..', 'caerp/static/js/build/')
}
if (process.env.CAERP_SOFT_NAME) {
  SOFT_NAME = process.env.CAERP_SOFT_NAME
} else {
  SOFT_NAME = 'CAERP'
}

console.log('Building js files in %s', BUILD_DIR)
console.log(APP_DIR)
const { VueLoaderPlugin } = require('vue-loader')

const config = {
  entry: {
    customer: path.join(APP_DIR, 'views', 'customer', 'customer.js'),
    company: path.join(APP_DIR, 'views', 'company', 'company.js'),
    company_map: path.join(APP_DIR, 'views', 'company_map', 'company_map.js'),
    notification: path.join(
      APP_DIR,
      'views',
      'notification',
      'notification.js'
    ),
    task_add: path.join(APP_DIR, 'views', 'task', 'add.js'),
    sale_files: path.join(APP_DIR, 'views', 'files', 'sale.js'),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        use: 'vue-loader',
      },
      {
        test: /\.(ts|js)$/,
        exclude: /node_modules/,
        use: 'babel-loader',
        resolve: { fullySpecified: false },
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        type: 'asset/resource',
        generator: {
          emit: false,
        },
      },
      // Catch the imports of .css files and injects them through a <link> tag
      {
        test: /\.css$/i,
         use: [
           {
             'loader': 'style-loader',
             'options': { injectType: "linkTag" },
           },
           'file-loader'
         ],
      },
    ],
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].js',
  },
  optimization: {
    splitChunks: {
      name: 'vendor-vue',
      chunks: 'all',
    },
  },
  plugins: [
    new VueLoaderPlugin(),
    // Pre-Provide datas used by other libraries
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      jquery: 'jquery',
    }),
  ],
  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
      }),
    ],
    modules: [APP_DIR, NODE_MODULES_DIR],
    extensions: ['.js', '.ts'],
    fullySpecified: false,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
}

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map'
    config.plugins.push(
      new webpack.DefinePlugin({
        DEBUG: true,
        SOFT_NAME: JSON.stringify(SOFT_NAME),
      })
    )
  }

  if (argv.mode === 'production') {
    // On ajoute un peu d'optimisation
    config.optimization.minimize = true
    config.plugins.push(
      new webpack.DefinePlugin({
        DEBUG: false,
        SOFT_NAME: JSON.stringify(SOFT_NAME),
      })
    )
    config.output.filename = '[name].min.js'
  }

  return config
}
