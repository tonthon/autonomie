/**
 * PaginationOption
 */
export interface IPaginationOption {
  page: number
  items_per_page: number
}
/**
 * Structure of options passed to collection load calls
 */
export interface ILoadOptions {
  fields?: Array<string>
  related?: Array<string>
  filters?: Object
  pageOptions?: IPaginationOptions
}
/**
 * Structure of data returned from collection load calls
 */
export interface IPaginationResponse {
  page: number
  per_page: number
  sort_by?: string
  order: string
  total_entries: number
}
export interface ICollectionResponse extends Array<Object> {}

export type PaginatedCollection = [IPaginationResponse, ICollectionResponse]
