import ProductForm from './ProductForm'
import TextAreaWidget from '../../../widgets/TextAreaWidget'
import InputWidget from '../../../widgets/InputWidget'
import SelectWidget from '../../../widgets/SelectWidget'

/** Abstract class to handle Form logic of fields from BaseQualiopiSaleProductForm
 *
 * Label/descriptions are kept in sync manually with python models. Take care of it <3.
 */
const BaseQualiopiSaleProductForm = ProductForm.extend({
    descriptionHelp: "Cette description se verra insérée dans le devis conjointement aux éléments de l'encadré « Formation »",
    descriptionTitle: "Description complémentaire",
    regions: _.defaults(
      {
          access_delay: '.field-access_delay',
          accessibility: '.field-accessibility',
          duration_hours: '.field-duration_hours',
          group_size: '.field-group_size',
          presence_modality: '.field-presence_modality',
          results: '.field-results',
          trainer: '.field-trainer',
          content: '.field-content',
          teaching_method: '.field-teaching_method',
      },
      ProductForm.prototype.regions
    ),
    trainingFields: {
        access_delay: {
            label: 'Délai d\'accès',
            description: 'Durée estimée entre la demande du bénéficiaire et le début de la prestation',
            required: true,
            widget: InputWidget
        },
        accessibility: {
            label: 'Accessibilité aux personnes handicapées',
            description: 'Accessibilité et politique d’accueil des personnes en situation de handicap',
            widget: TextAreaWidget
        },
        group_size: {
            label: 'Groupe / individuel',
            required: true,
            widget: SelectWidget,
            options: [] // initialized in initialize()
        },
        presence_modality: {
            label: 'Modalité de présence',
            required: true,
            widget: SelectWidget,
            options: [] // initialized in initialize()
        },
        duration_hours: {
            label: 'Durée en heures de la formation',
            required: true,
            widget: InputWidget,
            type: 'number'
        },
        content: {
            label: 'Contenu détaillé de la formation',
            description: 'Trame par étapes.',
            widget: TextAreaWidget,
            tinymce: true,
        },
        results: {
            label: 'Résultats',
            description: 'Taux de réussite, taux de satisfaction, données vérifiables, Taux d’obtention de certification.',
            required: true,
            widget: TextAreaWidget
        },
        teaching_method: {
            label: 'Les moyens pédagogiques utilisés',
            required: true,
            widget: TextAreaWidget,
            tinymce: true,
        },
        trainer: {
            label: 'Intervenant·e',
            required: true,
            widget: InputWidget
        },
    },
    initialize() {
        BaseQualiopiSaleProductForm.__super__.initialize.apply(this)
        this.trainingFields.presence_modality.options = this.config.request(
          'get:options',
          'presence_modalities'
        )
        this.trainingFields.group_size.options = this.config.request(
          'get:options',
          'group_sizes'
        )
    },
    onRender() {
        BaseQualiopiSaleProductForm.__super__.onRender.apply(this)
        if (this.training_form) {
            this.showTrainingFields();
        }
    },
    showTrainingFields() {
        let this_ = this
        _.each(this.trainingFields, function(field, key) {
            let options = _.clone(field)
            options['value'] = this_.model.get(key)
            options['field_name'] = key
            this_.showChildView(
              key,
              new field['widget'](options)
            )
        })
    }
})

export default BaseQualiopiSaleProductForm
