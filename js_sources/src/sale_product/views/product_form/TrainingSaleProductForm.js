import TextAreaWidget from '../../../widgets/TextAreaWidget'
import InputWidget from '../../../widgets/InputWidget'
import DateWidget from '../../../widgets/DateWidget'
import CheckboxWidget from '../../../widgets/CheckboxWidget'
import BaseQualiopiKnowledgeSaleProductForm from './BaseQualiopiKnowledgeSaleProductForm'

/** Handle Form logic of fields from TrainingSaleProductForm model
 *
 * Label/descriptions are kept in sync manually with python models. Take care of it <3.
 */
const TrainingSaleProductForm = BaseQualiopiKnowledgeSaleProductForm.extend({
    complexTitle: "Titre de la formation",
    regions: _.defaults(
      {
          prerequisites: '.field-prerequisites',
          modality_one: '.field-modality_one',
          modality_two: '.field-modality_two',
          rncp_rs_code: '.field-rncp_rs_code',
          certification_date: '.field-certification_date',
          certification_name: '.field-certification_name',
          certificator_name: '.field-certificator_name',
          gateways: '.field-gateways',
      },
      BaseQualiopiKnowledgeSaleProductForm.prototype.regions
    ),
    trainingFields: _.defaults({
        prerequisites: {
            label: 'Pré-requis obligatoires de la formation',
            required: true,
            widget: TextAreaWidget,
            tinymce: true,
        },
        rncp_rs_code: {
            label: 'Code RNCP/RS',
            description: 'Si formation certifiante uniquement, de forme RNCPXXXXX ou RSXXXX.',
            widget: InputWidget
        },
        certification_date: {
            label: 'Date de la certification',
            description: 'Si formation certifiante uniquement.',
            widget: DateWidget
        },
        certification_name: {
            label: 'Libellé de la certification',
            description: 'Si formation certifiante uniquement.',
            widget: InputWidget
        },
        certificator_name: {
            label: 'Nom du certificateur',
            description: 'Si formation certifiante uniquement.',
            widget: InputWidget
        },
        gateways: {
            label: 'Passerelles et débouchés',
            description: 'Si formation certifiante uniquement.',
            widget: InputWidget,
        },
        modality_one: {
            label: 'Formation intra-entreprise',
            widget: CheckboxWidget
        },
        modality_two: {
            label: 'Formation inter-entreprise',
            widget: CheckboxWidget
        }
    }, BaseQualiopiKnowledgeSaleProductForm.prototype.trainingFields),
})

export default TrainingSaleProductForm
