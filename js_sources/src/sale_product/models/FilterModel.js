/*
 * File Name :  FilterModel
 */
import Bb from 'backbone';

const FilterModel = Bb.Model.extend({
    defaults: {currentPage: 0}
});
export default FilterModel;
