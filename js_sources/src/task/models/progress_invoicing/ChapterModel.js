import BaseModel from 'base/models/BaseModel.js'
import ProductCollection from './ProductCollection';

const ChapterModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: ["id", "title", "description"],
    initialize(options) {
        ChapterModel.__super__.initialize.apply(this, arguments);
        this.populate();
    },
    populate() {
        // On ne crée la "sous-collection" que si le modèle ici a déjà un id
        if (this.get('id')) {
            if (!this.products) {
                this.products = new ProductCollection([], {
                    url: this.url() + '/' + "products"
                });
                this.products._parent = this;
            }
            this.products.fetch();
        }
    },
})
export default ChapterModel;